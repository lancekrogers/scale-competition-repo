FROM node:8.10-alpine
WORKDIR /home/app
COPY package.json .
RUN cat package.json
RUN apk update && apk upgrade && apk add --update alpine-sdk python python-dev && npm install
COPY . .
