# Scale Competition Contract

## Transaction type: submission 

## Methods

  - addQuestions [(example payload)](local/payloads/addQuestionsPayload.json)
  - submit [(example payload)](local/payloads/submitPayload.json)
  - review [(example payload)](local/payloads/reviewPayload.json)
  - addIndustries [(example payload)](local/payloads/addIndustries.json)
  - setContestStatus [(example payload)](local/payloads/setContestStatus.json)

## Steps to setup a contest
1. addQuestions
2. addIndustries
3. setContestStatus to Active
4. Users submit questions
5. Reviewers review questions
6. setContestStatus to Closed


## Heap structure
```
├── 1 (Competion 1)
    ├── applicants (userid to industry map) 
    ├── Energy (Project industry)
    │   ├── userid (submission from userid)
    │   └── contest (contains status information for each submission)
    ├── questions (Contains the questions for this particular competition)
    ├── contest (Contains the status information for each submission in the contest)
    └── status (Status of project "Active", "Pending" or "Closed")
```

## Status structure
```
{
    "approved": {
        "total": 0,
        "userids": []
    },
    "modify": {
        "total": 0,
        "userids": []
    },
    "rejected": {
        "total": 0,
        "userids": []
    },
    "submitted": {
        "total": 3,
        "userids": [
            "user1",
            "user2",
            "user3",
        ]
    },
    "total": 3
}
```

## Applicants structure
```
{ 
    "user1": "Energy",
    "user2": "Energy",
    "user3": "Energy"
}
```

## Questions structure
```
{
    "method": "addQuestions",
    "parameters": {
        "competitionId": "1000",
        "sections": [
            {
                "sectionId": 0,
                "header": "Do you qualify?",
                "message": "Questions to deteremine eligibilty",
                "canRepeat": false,
                "questions": [
                    {
                        "message": "UTILITY",
                        "questionId": 0,
                        "questionText": "Does your project use a utility token?",
                        "type": "binary",
                        "required": true
                    },
		    ...
		    {
                        "message": "DRAGONSCALE TOKEN ISSUANCE",
                        "questionId": 4,
                        "questionText": "Are willing to give away 10% of your token supply to participants?",
                        "type": "binary",
                        "required": true
                    }
                ]
            },
            {
                "sectionId": 1,
                "header": "About you",
                "message": "Questions to identify user submitting form",
                "canRepeat": false,
                "questions": [
                    {
                        "message": "User first name",
                        "questionId": 0,
                        "questionText": "First Name",
                        "required": true
                    },
                    {
                        "message": "User last name",
                        "questionId": 1,
                        "questionText": "Last Name",
                        "required": true
                    }
                ]
            },
            {
                "sectionId": 2,
                "header": "About your project",
                "message": "Questions about project being submitted",
                "canRepeat": false,
                "questions": [
                    {
                        "questionId": 0,
                        "message": "helper message",
                        "questionText": "Project/Company Name",
                        "type": "plainText",
                        "required": true
                    },
		    ...
		    {
                        "questionId": 5,
                        "message": "Enter a description of the project/company",
                        "questionText": "Description",
                        "type": "longForm",
                        "required": true
                    }
                ]
            },
            {
                "sectionId": 3,
                "header": "Team Details",
                "message": "Questions about the Project/Company team members",
                "canRepeat": true,
                "questions": [
                    {
                        "questionId": 0,
                        "message": "Enter team member first name",
                        "questionText": "First Name",
                        "type": "plainText"
                    },
                   ... 
                    {
                        "questionId": 4,
                        "message": "Enter team member LinkedIn name",
                        "questionText": "LinkedIn",
                        "type": "url"
                    } 
                ]
            }
        ]
    }
}

```


## Submission structure
```
{
    "answers": [
        {
            "answer": "true",
            "questionText": "Project Name"
        },
        {
            "answer": "true",
            "questionText": "utility token"
        },
        {
            "answer": "false",
            "questionText": "previousICO"
        },
	...
    ],
    "competitionId": "2",
    "eventLog": [
        {
            "event": "submit",
            "invoker": "73cc60c5-4e9d-462a-b17a-a9e6710ef1bb",
            "time": 1563552650380
        }
    ],
    "prjIndustry": "Energy",
    "status": "submitted",
    "userEmail": "lance@dragonchain.com",
    "userFullName": "Lance Rogers",
    "userId": "user1",
    "whitepaperFile": "Screen Shot 2019-01-22 at 10.13.28 AM.png"
}
```

# Industries Structure
```
[
"Energy",
"Materials",
...
"Financials",
"Information Technology"]
```


# TODO

- Add functionality to update valid industries (stretch goal)
