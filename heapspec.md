# Scale Competition Contract

## Transaction type: ds_whitepaper

## Methods

  - addQuestions [(example payload)](local/payloads/addQuestionsPayload.json)
  - submit [(example payload)](local/payloads/submitPayload.json)
  - review [(example payload)](local/payloads/reviewPayload.json)
  - addIndustries [(example payload)](local/payloads/addIndustries.json)

## Heap structure
```
├── 1 (Competion 1)
    ├── applicants (userid to industry map) 
    ├── Energy (Project industry)
    │   ├── userid (submission from userid)
    │   └── status (contains status information for each submission)
    ├── questions (Contains the questions for this particular competition)
    └── status (Contains the status information for each submission in the competion)
```

## Status structure
```
{
    "approved": {
        "total": 0,
        "userids": []
    },
    "modify": {
        "total": 0,
        "userids": []
    },
    "rejected": {
        "total": 0,
        "userids": []
    },
    "submitted": {
        "total": 3,
        "userids": [
            "user1",
            "user2",
            "user3",
        ]
    },
    "total": 3
}
```

## Applicants structure
```
{ 
    "user1": "Energy",
    "user2": "Energy",
    "user3": "Energy"
}
```

## Questions structure
```
[
    {
        "message": "helper message",
        "questionId": 0,
        "questionText": "Project Name",
	"type": "binary"
    },
    ... 
    {
        "message": "helper message",
        "questionId": 1,
        "questionText": "utility token",
	"type": "binary"
    },
    
]
```

## Submission structure
```
{
    "answers": [
        {
            "answer": "true",
            "questionText": "Project Name"
        },
        {
            "answer": "true",
            "questionText": "utility token"
        },
        {
            "answer": "false",
            "questionText": "previousICO"
        },
	...
    ],
    "competitionId": "2",
    "eventLog": [
        {
            "event": "submit",
            "invoker": "73cc60c5-4e9d-462a-b17a-a9e6710ef1bb",
            "time": 1563552650380
        }
    ],
    "prjIndustry": "Energy",
    "status": "submitted",
    "userEmail": "lance@dragonchain.com",
    "userFullName": "Lance Rogers",
    "userId": "user1",
    "whitepaperFile": "Screen Shot 2019-01-22 at 10.13.28 AM.png"
}
```

# Industries Structure
```
[
"Energy",
"Materials",
...
"Financials",
"Information Technology"]
```


# TODO

- Add method for updating the status at the competition level
- Status should be in ['Active', 'Pending', 'Closed']
- 'status' should be added to the status file
