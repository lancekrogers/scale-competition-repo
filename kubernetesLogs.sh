BEGINNING_OF_CONTRACT_ID=0a63b1f8

clear

kubes () {
  clear
  cluster_id=`kubectl get pods -n openfaas-fn | grep $BEGINNING_OF_CONTRACT_ID | cut -d ' ' -f 1`
  kubectl logs -f -n openfaas-fn $cluster_id
}

while true;
      do kubes; 
      sleep 30;
done;
