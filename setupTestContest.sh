#!/usr/bin/env bash

# Dctl must be installed with file input functionality

# Create competition
COMPETITION_ID=99999999999999999999999999999999998
TRANSACTION_TYPE=submission

addQuestionsPath="local/payloads/addQuestionsPayload.json"
addIndustriesPath="local/payloads/industriesPayload.json"
addSubmissionPath="local/payloads/submitPayload.json"
addReviewPath="local/payloads/reviewPayload.json"
setContestStatusPath="local/payloads/setContestStatus.json"
cancelContestPath="local/payloads/endContest.json"

declare -a path_array=($addQuestionsPath $addIndustriesPath $addSubmissionPath $addReviewPath $setContestStatusPath $cancelContestPath)


for jsonFile in "${path_array[@]}"
  do
  tmp=$(mktemp)
    cat $jsonFile |
    jq --arg COMPETITION_ID "$COMPETITION_ID" '.parameters.competitionId = $COMPETITION_ID' >  $tmp && mv $tmp $jsonFile 
  done

setupContest () {
  # add questions
  echo add questions
  dctl t c $TRANSACTION_TYPE $addQuestionsPath -f

  # add industries
  echo add industries
  dctl t c $TRANSACTION_TYPE $addIndustriesPath -f

  # set contest status
  echo set contest status
  dctl t c $TRANSACTION_TYPE $setContestStatusPath -f
}

functionalMethods () {
  # add submission
  echo add submission
  dctl t c $TRANSACTION_TYPE $addSubmissionPath -f

  # review submission
  echo add review
  dctl t c $TRANSACTION_TYPE $addReviewPath -f

  # Cancel test competition
  # echo cancel competition
  # dctl t c  $TRANSACTION_TYPE $cancelContestPath -f 

} 

setup () {
  setupContest
  functionalMethods
}

setup
