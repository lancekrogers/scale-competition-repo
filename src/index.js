const getStdin = require('get-stdin');
const handler = require('./whitepaper');
const { Logger } = require('./util');
require('dotenv').config();

const isArray = a => (!!a) && (a.constructor === Array);
const isObject = a => (!!a) && (a.constructor === Object);

getStdin().then(async (input) => {
  let payload;
  try {
    payload = JSON.parse(input);
  } catch (error) {
    return process.stdout.write(JSON.stringify({ error: 'Failed to parse input' }));
  }
  try {
    const response = await handler.main(payload);
    Logger.info('SUCCESS');
    Logger.info(response);

    if (isArray(response) || isObject(response)) {
      return process.stdout.write(JSON.stringify(response));
    }
    return process.stdout.write(response || '');
  } catch (error) {
    Logger.error('AN ERROR OCCURED');

    const response = {
      ok: false,
      error: { code: error.code, message: error.message },
    };
    return process.stdout.write(JSON.stringify(response));
  }
});
