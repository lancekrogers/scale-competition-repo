const sdk = require('dragonchain-sdk');
const { FailureByDesign, Logger } = require('../../util');

class DragonchainClient {
  constructor(injected = {}) {
    this.logger = injected.logger || Logger;
    this.DRAGONCHAIN_ID = injected.DRAGONCHAIN_ID || process.env.DRAGONCHAIN_ID;
    this.AUTH_KEY = injected.AUTH_KEY || process.env.AUTH_KEY;
    this.AUTH_KEY_ID = injected.AUTH_KEY_ID || process.env.AUTH_KEY_ID;
    this.ENDPOINT = injected.ENDPOINT || process.env.ENDPOINT;
    this.dragonchainClient = null;
  }

  async initialize() {
    this.dragonchainClient = await sdk.createClient({
      dragonchainId: this.DRAGONCHAIN_ID,
      endpoint: this.ENDPOINT,
      authKey: this.AUTH_KEY,
      authKeyId: this.AUTH_KEY_ID,
    });
  }

  async getSmartContractSecret(secretName) {
    try {
      const secret = await this.dragonchainClient.getSmartContractSecret({ secretName });
      return secret;
    } catch (e) {
      this.logger.error(e);
      throw new FailureByDesign('DRAGONCHAIN_UNEXPECTED_ERROR', 'An unexpected error occured');
    }
  }

  async getSmartContractObject(key) {
    try {
      const call = await this.dragonchainClient.getSmartContractObject({ key });
      return call;
    } catch (e) {
      this.logger.error(e);
      return { ok: false, status: 500 };
    }
  }

  async createBulkTransaction(transactionList) {
    try {
      const call = await this.dragonchainClient.createBulkTransaction({ transactionList });
      sdk.checkCallError(call);
      return call.response;
    } catch (e) {
      this.logger.error(e);
      throw new FailureByDesign('DRAGONCHAIN_UNEXPECTED_ERROR', 'An unexpected error occured');
    }
  }

  static checkCallError(call) {
    if (!call.ok) {
      if (call.status === 403) {
        throw new FailureByDesign('AUTHENTICATION_ERROR', call.response);
      }
      if (call.status === 404) {
        throw new FailureByDesign('NOT_FOUND', call.response);
      }
      if (call.status === 500) {
        throw new FailureByDesign('SERVER_ERROR', call.response);
      }
      // Catch-all for non 2XX http responses
      throw new FailureByDesign(
        'BAD_DRAGONCHAIN_RESPONSE',
        `Bad response from dragonchain. Status code: ${call.status} Response: ${JSON.stringify(call.response)}`,
      );
    }
  }
}

module.exports = DragonchainClient;

/**
 * All humans are welcome.
 */
