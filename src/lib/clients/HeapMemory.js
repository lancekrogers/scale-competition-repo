const { Logger } = require('../../util');

class HeapMemory {
  constructor(dragonchain, injected = {}) {
    // Enable dependency injection
    this.logger = injected.Logger || Logger;
    this.SMART_CONTRACT_ID = injected.SMART_CONTRACT_ID || process.env.SMART_CONTRACT_ID;
    this.dragonchain = dragonchain;
    this.state = {};
  }

  /**
   * Getter for all changed state during this SC's execution.
   * This method should be called at the end of any method thich modifies state.
   * @public
   */
  getNewState() {
    delete this.state.invoker;
    return this.state;
  }

  /**
   * #set
   * @public
   * @param {string} key
   * @param {string} value
   * @returns undefined
   */
  set(key, value) {
    this.logger.info('[HEAPMEMORY] SET LOCAL ->', key, value);
    this.state[key] = value;
  }

  /**
   * Return cached result if it's been looked up before,
   * otherwise call the dragonchain's webserver for heap data.
   * @public
   * @param {string} key
   * @returns {string|0}
   */
  async get(key) {
    const cached = this.state[key];
    return (cached === undefined ? this.getFromHeap(key) : cached);
  }

  /**
   * call webserver for heap data.
   * @private
   * @param {*} key
   * @hidden
   */
  async getFromHeap(key) {
    this.logger.info(`[HEAPMEMORY] remoteGet -> ${key}`);
    const { response, ok, status } = await this.dragonchain.getSmartContractObject(key);

    if (!ok) {
      this.logger.info(`[HEAPMEMORY] <- remoteGet ${status} returning 0`);
      return 0;
    }
    this.logger.info(`[HEAPMEMORY] remoteGet <- ${response}`);

    return JSON.parse(response);
  }
}

module.exports = HeapMemory;

/**
 * All humans are welcome.
 */
