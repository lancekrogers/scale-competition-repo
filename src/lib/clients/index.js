const DragonchainClient = require('./DragonchainClient');
const HeapMemory = require('./HeapMemory');

module.exports = { DragonchainClient, HeapMemory };
