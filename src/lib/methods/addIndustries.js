const SchemaValidator = require('../schemas/SchemaValidator');
const { Contest } = require('../models');


const addIndustries = async (heapMemory, parameters) => {
  await SchemaValidator.isValidIndustriesSchema(parameters);
  const {
    competitionId,
    validIndustries, // contains an array of industry strings
  } = parameters;

  await Contest.setValidIndustries(heapMemory, competitionId, validIndustries);

  return true;
};

module.exports = addIndustries;
