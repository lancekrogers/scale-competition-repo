const SchemaValidator = require('../schemas/SchemaValidator');
const { Contest } = require('../models');
const { Logger } = require('../../util');

const setContestStatus = async (heapMemory, parameters) => {
  await SchemaValidator.isValidContestStatusSchema(parameters); // throw INVALID_TRANSACTION

  const {
    competitionId,
    status,
  } = parameters;

  Logger.info(`METHOD setContestStatus ${status}`);

  const contest = new Contest(heapMemory, competitionId);
  await contest.setContestStatus(status);

  return true;
};

module.exports = setContestStatus;
