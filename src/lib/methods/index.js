const onSubmit = require('./onSubmit');
const onReview = require('./onReview');
const addQuestions = require('./questions.js');
const addIndustries = require('./addIndustries');
const setContestStatus = require('./contestStatus');

module.exports = {
  onSubmit, onReview, addQuestions, addIndustries, setContestStatus,
};
