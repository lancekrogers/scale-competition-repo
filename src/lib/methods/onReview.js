const SchemaValidator = require('../schemas/SchemaValidator');
const { Submission, Contest, Industry } = require('../models');
const { FailureByDesign, Logger } = require('../../util');

const onReview = async (heapMemory, parameters) => {
  Logger.info('METHOD onReview');
  await SchemaValidator.isValidReviewSchema(parameters); // throw INVALID_TRANSACTION
  const {
    competitionId,
    userId,
    newStatus, // (submitted, approved, rejected or modify)
    prjIndustry,
    comments,
  } = parameters;

  const submission = await Submission.getById(heapMemory, competitionId, prjIndustry, userId);

  if (submission.userId === undefined) {
    throw new FailureByDesign('KEY_NOT_FOUND', 'Key not found in HEAP');
  }

  const initialStatus = submission.status;

  await submission.setStatus(newStatus);
  submission.comments = comments;
  await submission.addEvent('review');

  const contest = await Contest.getContest(heapMemory, competitionId, initialStatus);
  await contest.remove(userId, initialStatus);

  const industry = await Industry.getStatus(
    heapMemory, competitionId, prjIndustry, initialStatus,
  );
  await industry.remove(userId, initialStatus);

  await contest.add(userId, prjIndustry, newStatus);

  await industry.add(userId, newStatus);

  return true;
};

module.exports = onReview;
