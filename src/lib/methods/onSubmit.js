const SchemaValidator = require('../schemas/SchemaValidator');
const { Submission, Contest, Industry } = require('../models');
const { FailureByDesign, Logger } = require('../../util');

const onSubmit = async (heapMemory, parameters) => {
  await SchemaValidator.isValidSubmitSchema(parameters); // throw INVALID_TRANSACTION

  const {
    competitionId,
    userId,
    userFullName,
    userEmail,
    prjIndustry,
    answers,
    whitepaperFile,
  } = parameters;

  const prevSubmission = await Submission.getById(heapMemory, competitionId, prjIndustry, userId);
  Logger.info(`Method OnSubmit.prevSubmission -> ${prevSubmission}`);

  const submission = prevSubmission === 0 ? new Submission(
    heapMemory,
    'submitted', // status
    competitionId,
    userId,
    userFullName,
    userEmail,
    whitepaperFile,
    prjIndustry,
    answers,
  ) : prevSubmission;

  Logger.info(`Method OnSubmit.post submission -> ${submission.asJson()}`);

  if (prevSubmission !== 0 && submission.status !== 'rejected' && submission.status !== 'modify') {
    Logger.info('EXISTING_SUBMISSION', 'User has already submitted a whitepaper.');
    throw new FailureByDesign('EXISTING_SUBMISSION', 'User has already submitted a whitepaper.');
  }

  await submission.addEvent('submit');

  const contest = await Contest.getContest(heapMemory, competitionId, 'submitted');
  await contest.add(userId, prjIndustry, 'submitted');

  const industry = await Industry.getStatus(heapMemory, competitionId, prjIndustry, 'submitted');
  await industry.add(userId, 'submitted');

  return true;
};

module.exports = onSubmit;
