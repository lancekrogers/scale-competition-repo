const SchemaValidator = require('../schemas/SchemaValidator');
const { Questions } = require('../models');


const addQuestions = async (heapMemory, parameters) => {
  await SchemaValidator.isValidQuestionsObjectSchema(parameters);
  const {
    competitionId,
    sections, // contains an array of question objects
  } = parameters;

  const questionObj = new Questions(
    heapMemory,
    competitionId,
    sections,
  );

  questionObj.addEvent('bulk-add-overwrite');

  return true;
};

module.exports = addQuestions;
