const { Logger } = require('../../util');

const formatKey = (competitionId, filename) => `${competitionId}/${filename}`;
const defualtIndustries = [
  'Energy',
  'Materials',
  'Industrials',
  'Consumer',
  'Healthcare',
  'Financials',
  'Information Technology',
  'Telecommunication Services',
  'Utilities',
  'Real Estate',
];


class ContestModel {
  constructor(
    heapMemory,
    competitionId,
    status = undefined, //  submitted, approved, rejected, modify
    contest = {
      total: 0,
      submitted: {
        total: 0,
        userids: [],
      },
      approved: {
        total: 0,
        userids: [],
      },
      rejected: {
        total: 0,
        userids: [],
      },
      modify: {
        total: 0,
        userids: [],
      },
    },
    contestStatus = 'Active', // Active, Pending, Closed
    validIndustries = defualtIndustries,
    applicants = {},
  ) {
    this.heapMemory = heapMemory;
    this.competitionId = competitionId;
    this.status = status;
    this.contest = contest;
    this.contestStatus = contestStatus;
    this.applicants = applicants;
    this.validIndustries = validIndustries;
  }

  async save() {
    this.heapMemory.set(formatKey(this.competitionId, 'contest'), this.asJson());
  }

  asJson() {
    return this.contest;
  }

  static async getContest(heapMemory, competitionId, status) {
    const contestKey = formatKey(competitionId, 'contest');
    Logger.info(`METHOD Contest.getContest key -> ${contestKey}`);
    const contestAsString = await heapMemory.get(contestKey);
    Logger.info(`METHOD Contest.getContest response -> ${JSON.stringify(contestAsString)}`);
    if (contestAsString === 0) {
      return new ContestModel(
        heapMemory,
        competitionId,
        status,
      );
    }
    return new ContestModel(
      heapMemory,
      competitionId,
      status,
      contestAsString,
    );
  }

  static async getValidIndustries(heapMemory, competitionId) {
    const key = formatKey(competitionId, 'validIndustries');
    Logger.info(`METHOD Contest.getValidIndustries => ${competitionId}`);
    const heapValidIndustries = await heapMemory.get(key);
    if (heapValidIndustries === 0) {
      return defualtIndustries;
    }
    return heapValidIndustries;
  }

  static async setValidIndustries(heapMemory, competitionId, validIndustries) {
    Logger.info(`METHOD Contest.setValidIndustries => ${competitionId}`);
    const key = formatKey(competitionId, 'industries');
    heapMemory.set(key, validIndustries);
  }

  static async getApplicants(heapMemory, competitionId) {
    const key = formatKey(competitionId, 'applicants');
    Logger.info(`METHOD Contest.getApplicants key -> ${key}`);
    const applicantsAsString = await heapMemory.get(key);
    Logger.info(`METHOD Contest.getApplicants response -> ${applicantsAsString}`);
    return applicantsAsString;
  }

  async setLocalApplicants(heapMemory, competitionId) {
    Logger.info(`Method Contest.setLocalApplicants => ${competitionId}`);
    const key = formatKey(competitionId, 'applicants');
    const applicants = await heapMemory.get(key);
    Logger.info(`METHOD Contest.setLocalApplicants => ${applicants}`);
    if (applicants === 0) {
      this.applicants = {};
    } else {
      this.applicants = applicants;
    }
  }

  async saveApplicants(applicants) {
    Logger.info(`METHOD Contest.saveApplicant -> ${applicants}`);
    Logger.info(`METHOD Contest.saveApplicant -> post set applicants ${applicants}`);
    this.heapMemory.set(formatKey(this.competitionId, 'applicants'), applicants);
  }

  async add(userId, prjIndustry, status) {
    Logger.info(`METHOD Contest.add -> ${userId} ${status}`);
    await this.setLocalApplicants(this.heapMemory, this.competitionId);
    Logger.info(`showme the money ${this.contest[status].userids.indexOf(userId)}`);
    if (this.contest[status].userids.indexOf(userId) <= 0) {
      this.contest[status].userids.push(userId);
      this.contest[status].total += 1;
      this.contest.total = this.calculateContestTotals();
      const apps = this.applicants;
      apps[userId] = prjIndustry;
      await this.save();
      await this.saveApplicants(apps);
    }
  }

  async remove(userId, status) {
    Logger.info(`METHOD Contest.remove -> ${userId}`);
    this.contest[status].userids.splice(this.contest[status].userids.indexOf(userId),
      1);
    this.contest[status].total -= 1;
    this.contest.total = this.calculateContestTotals();
    await this.save();
  }

  calculateContestTotals() {
    Logger.info('METHOD Contest.calculateContestTotals');
    const submittedTotal = this.contest.submitted.total;
    const approvedTotal = this.contest.approved.total;
    const rejectedTotal = this.contest.rejected.total;
    const modifyTotal = this.contest.modify.total;
    return submittedTotal + approvedTotal + rejectedTotal + modifyTotal;
  }

  async setContestStatus(status) {
    // Active, Pending, Closed
    Logger.info(`METHOD Contest.setCompetitionStatus -> ${status}`);
    this.contestStatus = status;
    await this.heapMemory.set(formatKey(this.competitionId, 'status'), status);
  }
}

module.exports = ContestModel;
