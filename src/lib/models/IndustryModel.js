const formatKey = (competitionId, prjIndustry) => `${competitionId}/${prjIndustry}/status`;
const { Logger } = require('../../util');

class IndustryContestModel {
  constructor(
    heapMemory,
    competitionId,
    prjIndustry,
    status = undefined, //  submitted, approved, rejected, modify
    contest = {
      total: 0,
      submitted: {
        total: 0,
        userids: [],
      },
      approved: {
        total: 0,
        userids: [],
      },
      rejected: {
        total: 0,
        userids: [],
      },
      modify: {
        total: 0,
        userids: [],
      },
    },
  ) {
    this.heapMemory = heapMemory;
    this.competitionId = competitionId;
    this.prjIndustry = prjIndustry;
    this.status = status;
    this.contest = contest;
  }

  async save() {
    this.heapMemory.set(formatKey(this.competitionId, this.prjIndustry),
      this.asJson());
  }

  asJson() {
    return this.contest;
  }

  static async getStatus(heapMemory, competitionId, prjIndustry, status) {
    const contestKey = formatKey(competitionId, prjIndustry, 'contest');
    Logger.info(`METHOD IndustryModel.getStatus key -> ${contestKey}`);
    const contestAsString = await heapMemory.get(contestKey);
    Logger.info(`METHOD IndustryModel.getStatus response -> ${contestAsString}`);
    if (contestAsString === 0) {
      return new IndustryContestModel(
        heapMemory,
        competitionId,
        prjIndustry,
        status,
      );
    }
    return new IndustryContestModel(
      heapMemory,
      competitionId,
      prjIndustry,
      status,
      contestAsString,
    );
  }

  async add(userId, status) {
    Logger.info(`METHOD Industry.add -> ${userId}`);
    if (this.contest[status].userids.indexOf(userId) <= 0) {
      this.contest[status].userids.push(userId);
      this.contest[status].total += 1;
      this.contest.total = this.calculateIndustryTotals();
      await this.save();
    }
  }

  async remove(userId, status) {
    Logger.info(`METHOD Industry.remove -> ${userId}`);
    this.contest[status].userids.splice(this.contest[status].userids.indexOf(userId),
      1);
    this.contest[this.status].total -= 1;
    this.contest.total = this.calculateIndustryTotals();
    await this.save();
  }

  calculateIndustryTotals() {
    Logger.info('METHOD Contest.calculateContestTotals');
    const submittedTotal = this.contest.submitted.total;
    const approvedTotal = this.contest.approved.total;
    const rejectedTotal = this.contest.rejected.total;
    const modifyTotal = this.contest.modify.total;
    return submittedTotal + approvedTotal + rejectedTotal + modifyTotal;
  }
}

module.exports = IndustryContestModel;
