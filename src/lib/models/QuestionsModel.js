const formatKey = competitionId => `${competitionId}/questions`;
const { Logger } = require('../../util');


class QuestionsModel {
  constructor(
    heapMemory,
    competitionId,
    sections = [],
    eventLog = [],
  ) {
    this.heapMemory = heapMemory;
    this.competitionId = competitionId;
    this.sections = sections;
    this.eventLog = eventLog;
  }

  async save() {
    this.heapMemory.set(formatKey(this.competitionId), this.asJson());
  }

  asJson() {
    return this.sections;
  }

  //  async getByCompetition(heapMemory, competitionId) {
  //    // Returns all sections
  //    const questionsAsString = await heapMemory.get(formatKey(competitionId));
  //    const sections = questionsAsString;

  //    return new QuestionsModel(
  //      heapMemory,
  //      competitionId,
  //      sections,
  //    );
  //   }

  async addEvent(event) {
    Logger.info(`METHOD QuestionModel.addEvent -> ${event}`);
    const time = new Date().valueOf();
    const invoker = await this.heapMemory.get('invoker');
    this.eventLog.push({ event, invoker, time });
    await this.save();
  }

  // async addQuestion(question, id=undefined, update=false) {
  //   // Adds one question at a specified id
  //   var question = JSON.parse(JSON.stringify(question));

  //   if (this.sections[id] == undefined) {
  //     // if id is blank or index is too large add question to end
  //     // of sections array
  //     await this.sections.push(question);
  //     await this.save();
  //   }
  //   else if (update !== false) {
  //     this.sections[id] = question;
  //     await this.save();
  //   }
  //   else {
  //     this.quesitons.splice(id, 0, question)
  //     // For each element after the newly added element
  //     // increment the questionId on each question objerct by 1
  //     for (let item of this.sections.slice(id + 1, this.sections.length)) {
  //       await item['questionId']++;
  //       await this.save();
  //     }
  //   }
  // }

  // async removeQuestion(id) {
  //   // Removes a question by id
  //   this.sections.splice(id, 1);
  //   for (let item of this.quesitons.slice(id, this.sections.length)) {
  //     item['questionId']--;
  //     await this.save();
  //     }
  // }
}

module.exports = QuestionsModel;
