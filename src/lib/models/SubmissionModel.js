const formatKey = (competitionId, prjIndustry, userId) => `${competitionId}/${prjIndustry}/${userId}`;
const { Logger } = require('../../util');

class SubmissionModel {
  constructor(
    heapMemory,
    status = undefined, // submitted, approved, rejected or modify
    competitionId = undefined,
    userId = undefined,
    userFullName = undefined,
    userEmail = undefined,
    whitepaperFile = undefined,
    prjIndustry = undefined,
    answers = [],
    eventLog = [],
    comments = undefined,
  ) {
    this.heapMemory = heapMemory;
    this.status = status;
    this.competitionId = competitionId;
    this.userId = userId;
    this.userFullName = userFullName;
    this.userEmail = userEmail;
    this.whitepaperFile = whitepaperFile;
    this.prjIndustry = prjIndustry;
    this.answers = answers;
    this.eventLog = eventLog;
    this.comments = comments;
  }

  async addEvent(event) {
    Logger.info(`METHOD submission.addEvent -> ${event}`);
    const time = new Date().valueOf();
    const invoker = await this.heapMemory.get('invoker');
    this.eventLog.push({ event, invoker, time });
    await this.save();
  }

  async save() {
    Logger.info('METHOD SubmissionModel.save');
    const key = formatKey(this.competitionId, this.prjIndustry, this.userId);
    this.heapMemory.set(key, this.asJson());
  }

  asJson() {
    return {
      status: this.status,
      competitionId: this.competitionId,
      userId: this.userId,
      userFullName: this.userFullName,
      userEmail: this.userEmail,
      whitepaperFile: this.whitepaperFile,
      prjIndustry: this.prjIndustry,
      answers: this.answers,
      eventLog: this.eventLog,
    };
  }

  static async getById(heapMemory, competitionId, prjIndustry, userId) {
    const submissionKey = formatKey(competitionId, prjIndustry, userId);
    Logger.info(`METHOD SubmissionModel.getById key -> ${submissionKey}`);
    const prevSubmission = await heapMemory.get(submissionKey);
    Logger.info(`METHOD SubmissionModel.getById response -> ${prevSubmission} type ${typeof prevSubmission}`);

    const submission = prevSubmission !== 0
      ? new SubmissionModel(
        heapMemory,
        prevSubmission.status,
        prevSubmission.competitionId,
        prevSubmission.userId,
        prevSubmission.userFullName,
        prevSubmission.userEmail,
        prevSubmission.whitepaperFile,
        prevSubmission.prjIndustry,
        prevSubmission.answers,
        prevSubmission.eventLog,
      ) : prevSubmission;

    return submission;
  }

  async setStatus(newStatus) {
    Logger.info(`METHOD SubmissionModel.setStatus -> ${newStatus}`);
    this.status = newStatus;
    Logger.info(`METHOD SubmissionModel.save heapmemory: ${this.heapMemory}`);
    await this.save();
  }
}

module.exports = SubmissionModel;
