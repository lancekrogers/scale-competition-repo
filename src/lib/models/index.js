const Submission = require('./SubmissionModel');
const Contest = require('./ContestModel');
const Industry = require('./IndustryModel');
const Questions = require('./QuestionsModel');

module.exports = {
  Submission, Contest, Industry, Questions,
};
