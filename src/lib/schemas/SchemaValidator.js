const Ajv = require('ajv');

const { FailureByDesign } = require('../../util');

const ajv = new Ajv();

const {
  containsValidPayload,
  submitSchema,
  reviewSchema,
  questionsObjectSchema,
  validIndustriesSchema,
  validContestStatusSchema,
} = require('./Schemas');

const validateSchema = (schema, thingToValidate) => {
  if (!ajv.validate(schema, thingToValidate)) {
    throw new FailureByDesign('INVALID_SCHEMA', 'Invalid schema');
  }
  return true;
};

class SchemaValidator {
  static isValidEventPayload(eventPayload) {
    return validateSchema(containsValidPayload, eventPayload);
  }

  static isValidSubmitSchema(submitPayload) {
    return validateSchema(submitSchema, submitPayload);
  }

  static isValidReviewSchema(reviewPayload) {
    return validateSchema(reviewSchema, reviewPayload);
  }

  static isValidQuestionsObjectSchema(questionsPayload) {
    // Used for validating a questions object
    // (array of questions)
    return validateSchema(questionsObjectSchema, questionsPayload);
  }

  static isValidIndustriesSchema(industriesPayload) {
    return validateSchema(validIndustriesSchema, industriesPayload);
  }

  static isValidContestStatusSchema(contestStatusPayload) {
    return validateSchema(validContestStatusSchema, contestStatusPayload);
  }
}

module.exports = SchemaValidator;
