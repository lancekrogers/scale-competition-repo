const validMethods = ['submit', 'review', 'addQuestions', 'getQuestions', 'addIndustries',
  'setContestStatus'];
const validContest = ['approved', 'rejected', 'modify'];
const validQuestionTypes = ['binary', 'plainText', 'longForm', 'dropDown', 'url'];
const validIndustries = [
  'Energy',
  'Materials',
  'Industrials',
  'Consumer',
  'Healthcare',
  'Financial Services',
  'Information Technology',
  'Telecommunication Services',
  'Utilities',
  'Real Estate',
];
const validContestStatus = ['Active', 'Pending', 'Closed'];


const containsValidPayload = {
  title: 'containsValidPayload',
  type: 'object',
  properties: {
    payload: {
      type: 'object',
      properties: {
        method: {
          enum: validMethods,
        },
        parameters: {
          type: 'object',
        },
      },
      required: ['method', 'parameters'],
    },
  },
  required: ['payload'],
};

// const prjTeamObjectSchema = {
// type: 'object',
// properties: {
// name: { type: 'string' },
// role: { type: 'string' },
// linkedIn: { type: 'string' },
// },
// required: ['name', 'role', 'linkedIn'],
// };

// const prjTeamArraySchema = {
// type: 'array',
// items: prjTeamObjectSchema,
// };

const answerSchema = {
  type: 'object',
  properties: {
    questionText: { type: 'string' },
    answer: { type: 'string' },
  },
  requred: ['questionText', 'answer'],
};

const submitSchema = {
  title: 'submit',
  type: 'object',
  properties: {
    competitionId: { type: 'string' },
    userId: { type: 'string' },
    userFullName: { type: 'string' },
    userEmail: { type: 'string' },
    prjIndustry: {
      enum: validIndustries,
    },
    whitepaperFile: { type: 'string' },
    answers: {
      type: 'array',
      items: answerSchema,
    },
  },
  required: [
    'competitionId',
    'userId',
    'userFullName',
    'userEmail',
    'prjIndustry',
    'whitepaperFile',
    'answers',
  ],
};

const reviewSchema = {
  title: 'review',
  type: 'object',
  properties: {
    userId: { type: 'string' },
    competitionId: { type: 'string' },
    prjIndustry: {
      enum: validIndustries,
    },
    newStatus: {
      enum: validContest,
    },
    comments: { type: 'string' },
  },
  required: ['userId', 'competitionId', 'prjIndustry', 'newStatus'], // , 'reason']
};

const questionSchema = {
  //  Individual question schema
  title: 'question',
  type: 'object',
  properties: {
    questionId: { type: 'number' },
    questionText: { type: 'string' },
    message: { type: 'string' },
    required: { type: 'boolean' },
    dropDownItems: { type: 'array' },
    type: {
      enum: validQuestionTypes,
    },
  },
};

const questionsArraySchema = {
  // Full question array
  title: 'questions',
  type: 'array',
  items: questionSchema,
};

const sectionsSchema = {
  title: 'section',
  type: 'object',
  properties: {
    sectionId: { type: 'number' },
    header: { type: 'string' },
    message: { type: 'string' },
    canRepeat: { type: 'boolean' },
    questions: questionsArraySchema,
  },
  required: ['questions', 'header'],
};

const sectionsArraySchema = {
  title: 'sections',
  type: 'array',
  items: sectionsSchema,
};

const questionsObjectSchema = {
  title: 'questions',
  type: 'object',
  properties: {
    competitionId: { type: 'string' },
    sections: sectionsArraySchema,
  },
  required: ['competitionId', 'sections'],
};

const validIndustriesSchema = {
  title: 'validIndustries',
  type: 'object',
  properties: {
    competitionId: { type: 'string' },
    validIndustries: { type: 'array' },
  },
};

const validContestStatusSchema = {
  title: 'validContestStatus',
  type: 'object',
  properties: {
    competitionId: { type: 'string' },
    status: { enum: validContestStatus },
  },
};

module.exports = {
  submitSchema,
  reviewSchema,
  containsValidPayload,
  questionsObjectSchema,
  validIndustriesSchema,
  validContestStatusSchema,
};
