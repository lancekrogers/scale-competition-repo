module.exports = class FailureByDesign extends Error {
  constructor(code, message) {
    super(message);
    this.name = 'FailureByDesign';
    this.message = message || 'Failure By Design';
    this.code = code || 'FAILURE_BY_DESIGN';
  }
};
