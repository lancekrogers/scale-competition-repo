module.exports = {
  info: (...log) => { if (process.env.LOG_LEVEL !== 'OFF') process.stderr.write(`${JSON.stringify(...log)}\n`); },
  trace: (...log) => { if (process.env.LOG_LEVEL !== 'OFF') process.stderr.write(`${JSON.stringify(...log)}\n`); },
  error: (...log) => { if (process.env.LOG_LEVEL !== 'OFF') process.stderr.write(`${JSON.stringify(...log)}\n`); },
  debug: (...log) => { if (process.env.LOG_LEVEL !== 'OFF') process.stderr.write(`${JSON.stringify(...log)}\n`); },
};
