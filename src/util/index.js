const FailureByDesign = require('./Errors');
const Logger = require('./Logger');

module.exports = { FailureByDesign, Logger };
