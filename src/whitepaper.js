const { DragonchainClient, HeapMemory } = require('./lib/clients');
const schemaValidator = require('./lib/schemas/SchemaValidator');
const {
  onSubmit, onReview, addQuestions, addIndustries, setContestStatus,
} = require('./lib/methods');
const { Logger } = require('./util');

module.exports.main = async function main(event) {
  schemaValidator.isValidEventPayload(event); // throws INVALID_SCHEMA

  const dragonchain = new DragonchainClient();
  await dragonchain.initialize();

  const heapMemory = new HeapMemory(dragonchain);

  const {
    header: { txn_id: invoker },
    payload: { method, parameters },
  } = event;

  await heapMemory.set('invoker', invoker);

  const status = await {
    submit: async () => onSubmit(heapMemory, parameters),
    review: async () => onReview(heapMemory, parameters),
    addQuestions: async () => addQuestions(heapMemory, parameters),
    addIndustries: async () => addIndustries(heapMemory, parameters),
    setContestStatus: async () => setContestStatus(heapMemory, parameters),
  }[method]();

  Logger.info('METHOD STATUS ->', status);

  const newHeapState = heapMemory.getNewState();
  Logger.info(JSON.stringify(newHeapState));
  return newHeapState;
};
